import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import { Layout } from "antd";
import SideBar from "./components/layouts/SideBar";
import Breadcrmb from "./components/layouts/BreadCrmb";
import "./App.css";
import Sale from "./components/sale/Sale";
import NavBar from "./components/layouts/Navbar";
import Home from "./components/Home";

const { Content } = Layout;

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Layout>
          <Layout>
            <SideBar />
            <Layout style={{ padding: "0 24px 24px" }}>
              <Breadcrmb />
              <Content className="content">
                <Home />
              </Content>
            </Layout>
          </Layout>
        </Layout>
      </BrowserRouter>
    );
  }
}

export default App;
