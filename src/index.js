import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Router, Switch, Route } from "react-router-dom";
import createBrowserHistory from "history/createBrowserHistory";

import "bootstrap/scss/bootstrap.scss";
import "antd/dist/antd.css";

import "./index.css";
import NavBar from "./components/layouts/Navbar";
import SaleBoard from "./components/sale/SaleBoard";
import ManufacturerBoard from "./components/manufacturer/ManufacturerBoard";
import { Layout } from "antd";

const history = createBrowserHistory();

ReactDOM.render(
  <Router history={history}>
    <Switch>
      <Route
        exact
        path="/"
        render={() => (
          <Layout>
            <NavBar />
            <App />
          </Layout>
        )}
      />
      <Route
        path="/saleboard"
        render={() => (
          <Layout>
            <NavBar />
            <SaleBoard />
          </Layout>
        )}
      />
      <Route
        path="/manufacturerboard"
        render={() => (
          <Layout>
            <NavBar />
            <ManufacturerBoard />
          </Layout>
        )}
      />
    </Switch>
  </Router>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
