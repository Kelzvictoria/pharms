import React from "react";
import { Layout, Menu } from "antd";
import { Link, NavLink } from "react-router-dom";
import { Avatar, Icon } from "antd";

const { Header } = Layout;

const NavBar = ({ children }) => {
  return (
    <Header className="header">
      <div className="logo" />
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={["1"]}
        style={{ lineHeight: "64px" }}
      >
        <Menu.Item key="1">
          <Link to="/">Home</Link>{" "}
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="/SaleBoard">Sales</Link>
        </Menu.Item>
        <Menu.Item key="3">
          <Link to="/ManufacturerBoard">Manufacturers</Link>
        </Menu.Item>

        <Menu.Item key="4" style={{ float: "right" }}>
          <NavLink to="/">
            <Avatar style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}>
              VK
            </Avatar>
          </NavLink>
        </Menu.Item>
      </Menu>
      {children}
    </Header>
  );
};

export default NavBar;
