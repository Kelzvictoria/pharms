import React, { Component } from "react";
import { Layout, Menu, Icon } from "antd";
import { Link } from "react-router-dom";

const { SubMenu } = Menu;
const { Sider } = Layout;

const SideBar = () => {
  return (
    <Sider width={200} style={{ background: "#fff" }}>
      <Menu
        mode="inline"
        defaultSelectedKeys={["1"]}
        defaultOpenKeys={["sub1"]}
        style={{ height: "100%", borderRight: 0 }}
      >
        <SubMenu
          key="sub1"
          title={
            <span>
              <Icon type="home" />
              <Link to="/">PMS</Link>
            </span>
          }
        >
          <Menu.Item key="1">Details</Menu.Item>
        </SubMenu>
        <SubMenu
          key="sub2"
          title={
            <span>
              <Icon type="account-book" />
              Sales
            </span>
          }
        >
          <Menu.Item key="5">
            <Link to="/SaleForm">Record Sale</Link>
          </Menu.Item>
          <Menu.Item key="6">
            <Link to="/SalesReport">Report</Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu
          key="sub3"
          title={
            <span>
              <Icon type="plus-circle" />
              Manufacturers
            </span>
          }
        >
          <Menu.Item key="9">
            <Link to="/ManufacturerForm">Add Manufacturer</Link>
          </Menu.Item>
          <Menu.Item key="10">
            <Link to="/Manufacturers">List of Manufacturers</Link>
          </Menu.Item>
        </SubMenu>

        <SubMenu
          key="sub2"
          title={
            <span>
              <Icon type="stock" />
              Stock
            </span>
          }
        >
          <Menu.Item key="5">
            <Link to="StockForm">Add Stock</Link>
          </Menu.Item>
          <Menu.Item key="6">
            <Link to="/StockList">List Stock</Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu
          key="sub3"
          title={
            <span>
              <Icon type="user" />
              Staff
            </span>
          }
        >
          <Menu.Item key="9">
            <Link to="/StaffForm">New Staff</Link>
          </Menu.Item>
          <Menu.Item key="10">
            <Link to="/StaffList">View all staff</Link>
          </Menu.Item>
        </SubMenu>
      </Menu>
    </Sider>
  );
};

export default SideBar;
