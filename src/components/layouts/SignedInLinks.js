import React, { Component } from 'react';
import { Layout, Menu} from 'antd';
import {Link, NavLink} from 'react-router-dom';
import { Avatar, Icon } from 'antd';

const { Header } = Layout;

const SignedInLinks = () => {
    return ( 
            <span>
                <Menu.Item key="2"><NavLink to = "/">Sales</NavLink></Menu.Item>
                <Menu.Item key="3"><NavLink to = "/">Manufacturers</NavLink></Menu.Item>

                <Menu.Item key="4" style={{ float:"right"}}><NavLink to = "/"><Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>U</Avatar></NavLink></Menu.Item>
            </span>
            
     );
}

export default SignedInLinks;