import React, { Component } from 'react';
import { Layout, Menu} from 'antd';
import {Link, NavLink} from 'react-router-dom';

const { Header } = Layout;

const SignedOutLinks = () => {
    return ( 
            <div>
                <Menu.Item key="2"><Link to = "/">Sign in</Link></Menu.Item>
                <Menu.Item key="3"><Link to = "/">Sign up</Link></Menu.Item>
            </div>
     );
}

export default SignedOutLinks;