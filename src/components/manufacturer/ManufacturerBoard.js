import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import { Layout } from "antd";
import SideBar from "../layouts/SideBar";
import BreadCrmb from "../layouts/BreadCrmb";
import ManufacturerForm from "./ManufacturerForm";

const { Content } = Layout;

class ManufacturerBoard extends Component {
  render() {
    return (
      <BrowserRouter>
        <Layout>
          <Layout>
            <SideBar />
            <Layout style={{ padding: "0 24px 24px" }}>
              <BreadCrmb />
              <Content className="content">
                <ManufacturerForm />
              </Content>
            </Layout>
          </Layout>
        </Layout>
      </BrowserRouter>
    );
  }
}

export default ManufacturerBoard;
