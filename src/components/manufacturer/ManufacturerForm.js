import React, { Component } from 'react';

import {
  Form,
  Select,
  InputNumber,
  Switch,
  Radio,
  Slider,
  Button,
  Upload,
  Icon,
  Rate,
  Checkbox,
  Row,
  Col,
  Input
} from "antd";

const { Option } = Select;

class ManufacturerForm extends Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
      }
    });
  };

  normFile = e => {
    console.log("Upload event:", e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  state = {};
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form layout="vertical" onSubmit={this.handleSubmit}>
        <Form.Item label="">
          <h3 className="ant-form-text">Add Manufacturer</h3>
        </Form.Item>
        
        <Row gutter={[16,16]}>
          <Col span ={8}>
            <Form.Item label="Manufacturer Name">
                {getFieldDecorator('manufacturer_name', {
                  rules: [
                    {
                      required: true,
                      message: 'Please input manufacturer name',
                    },
                  ],
                })(<Input placeholder="Please input manufacturer name" />)}
            </Form.Item>
          </Col>

          <Col span ={8}>
            <Form.Item label="Drug">
              {getFieldDecorator('drug', {
                rules: [
                  {
                    required: true,
                    message: 'Please input drug name',
                  },
                ],
              })(<Input placeholder="Please input drug name" />)}
            </Form.Item>
          </Col>

          <Col span ={8}>
            <Form.Item label="Manufacturer Address">
              {getFieldDecorator('manufacturer_address')(<Input placeholder="Please input manufacturer address" />)}
            </Form.Item>
          </Col>

          <Col span ={8}>
          <Form.Item label="Manufacturer Email">
            {getFieldDecorator('manufacturer_email')(<Input placeholder="Please input manufacturer email" />)}
          </Form.Item>
          </Col>

          <Col span ={8}>
            <Form.Item label="Contact Name">
              {getFieldDecorator('contact_name')(<Input placeholder="Please input contact name" />)}
            </Form.Item>
          </Col>

          <Col span ={8}>
          <Form.Item label="Contact Email">
            {getFieldDecorator('contact_email')(<Input placeholder="Please input contact email" />)}
          </Form.Item>
          </Col>

          <Col span = {8}>
            <Form.Item label="Phone Number">
              {getFieldDecorator('manufacturer_phone')(<Input placeholder="Please input manufacturer phone" />)}
            </Form.Item>
          </Col>
          <br/>
          <Col span ={24}>
            <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
          </Col>
        </Row>

      </Form>
    );
  }
}

export default Form.create() (ManufacturerForm);
