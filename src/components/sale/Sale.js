import React, { Component } from "react";
import { Form, Button, Switch, Select, Row, Col, Table, Divider } from "antd";

const { Option } = Select;
const columns = [
  {
    title: "S/N",
    dataIndex: "sn",
    key: "sn"
  },
  {
    title: "Item",
    dataIndex: "item",
    key: "item",
    render: text => <a>{text}</a>
  },
  {
    title: "Quantity",
    dataIndex: "quantity",
    key: "quantity"
  },
  {
    title: "Price",
    dataIndex: "price",
    key: "price"
  },
  {
    title: "Action",
    key: "action",
    render: (text, record) => (
      <span>
        <a>Invite {record.name}</a>
        <Divider type="vertical" />
        <a>Delete</a>
      </span>
    )
  }
];
const data = [{}, {}, {}];

class Sale extends Component {
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Form layout="vertical">
          <Form.Item label="Regular customer?">
            {getFieldDecorator("switch", { valuePropName: "checked" })(
              <Switch />
            )}
          </Form.Item>

          <Row gutter={[16, 16]}>
            <Col span={12}>
              <Form.Item label="Customer" hasFeedback>
                {getFieldDecorator("select", {
                  rules: [{ required: false, message: "Select customer" }]
                })(
                  <Select placeholder="Select customer">
                    <Option value="china">Victoria Kazeem</Option>
                    <Option value="usa">Chinedu Okorie</Option>
                    <Option value="usa">Omotayo Adegoke</Option>
                    <Option value="usa">Abubakar Shekoni</Option>
                  </Select>
                )}
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item label="Item(s) Purchased">
                {getFieldDecorator("select-multiple", {
                  rules: [
                    {
                      required: true,
                      message: "Please select item purchased",
                      type: "array"
                    }
                  ]
                })(
                  <Select mode="multiple" placeholder="Select item purchased">
                    <Option value="paracetamol">Emzor Paracetamol</Option>
                    <Option value="aboniki balm">Aboniki Balm</Option>
                    <Option value="boska">Boska</Option>
                    <Option value="coartem">Coartem</Option>
                  </Select>
                )}
              </Form.Item>
            </Col>
          </Row>

          <Table columns={columns} /* dataSource={data}*/ />
          <br />
          <Form.Item>
            <Button type="primary">Submit</Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default Form.create()(Sale);
