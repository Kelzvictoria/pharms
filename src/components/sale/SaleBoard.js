import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import { Layout } from "antd";
import SideBar from "../layouts/SideBar";
import BreadCrmb from "../layouts/BreadCrmb";
import Sale from "./Sale";

const { Content } = Layout;

class SaleBoard extends Component {
  render() {
    return (
      <BrowserRouter>
        <Layout>
          <Layout>
            <SideBar />
            <Layout style={{ padding: "0 24px 24px" }}>
              <BreadCrmb />
              <Content className="content">
                <Sale />
              </Content>
            </Layout>
          </Layout>
        </Layout>
      </BrowserRouter>
    );
  }
}

export default SaleBoard;
