import React, { Component } from "react";

import {
  Form,
  Select,
  InputNumber,
  Switch,
  Radio,
  Slider,
  Button,
  Upload,
  Icon,
  Rate,
  Checkbox,
  Row,
  Col,
  message,
  Input
} from "antd";

const { Option } = Select;

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error("Image must smaller than 2MB!");
  }
  return isJpgOrPng && isLt2M;
}

class PharmacyForm extends Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
      }
    });
  };

  normFile = e => {
    console.log("Upload event:", e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  state = {};

  handleChange = info => {
    if (info.file.status === "uploading") {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false
        })
      );
    }
  };

  render() {
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? "loading" : "plus"} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    const { imageUrl } = this.state;
    const { getFieldDecorator } = this.props.form;
    return (
      <Form layout="vertical" onSubmit={this.handleSubmit}>
        <Form.Item label="">
          <h3 className="ant-form-text">Add Pharmacy</h3>
        </Form.Item>

        <Row gutter={[16, 16]}>
          <Col span={8}>
            <Form.Item label="Pharmacy Name">
              {getFieldDecorator("pharmacy_name", {
                rules: [
                  {
                    required: true,
                    message: "Please input Pharmacy name"
                  }
                ]
              })(<Input placeholder="Please input Pharmacy name" />)}
            </Form.Item>
          </Col>

          <Col span={8}>
            <Form.Item label="Pharmacy Address">
              {getFieldDecorator("pharmacy_address")(
                <Input placeholder="Please input Pharmacy address" />
              )}
            </Form.Item>
          </Col>

          <Col span={8}>
            <Form.Item label="Pharmacy Email">
              {getFieldDecorator("pharmacy_email")(
                <Input placeholder="Please input Pharmacy email" />
              )}
            </Form.Item>
          </Col>

          <Col span={8}>
            <Form.Item label="Contact Name">
              {getFieldDecorator("contact_name")(
                <Input placeholder="Please input contact name" />
              )}
            </Form.Item>
          </Col>

          <Col span={8}>
            <Form.Item label="Contact Email">
              {getFieldDecorator("contact_email")(
                <Input placeholder="Please input contact email" />
              )}
            </Form.Item>
          </Col>

          <Col span={8}>
            <Form.Item label="Phone Number">
              {getFieldDecorator("pharmacy_phone")(
                <Input placeholder="Please input Pharmacy phone" />
              )}
            </Form.Item>
          </Col>
          <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            beforeUpload={beforeUpload}
            onChange={this.handleChange}
          >
            {imageUrl ? (
              <img src={imageUrl} alt="avatar" style={{ width: "100%" }} />
            ) : (
              uploadButton
            )}
          </Upload>
          <br />
          <Col span={24}>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(PharmacyForm);
