import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import { Layout } from "antd";
import Navbar from "../layouts/Navbar";
import SideBar from "../layouts/SideBar";
import Breadcrmb from "../layouts/BreadCrmb";
import AntTest from "../AntTest";
import SignedInLinks from "../layouts/SignedInLinks";
import Sale from "../sale/Sale";
import ManufacturerForm from "../manufacturer/ManufacturerForm";
import PharmacyForm from "../pharmacy/PharmacyForm";
import Staff from "../staff/Staff";
import StockForm from "../stock/StockForm";

const { Content } = Layout;

const DashBoard = () => {
  return (
    <BrowserRouter>
      <Layout>
        <Navbar />
        <Layout>
          <SideBar />
          <Layout style={{ padding: "0 24px 24px" }}>
            <Breadcrmb />
            <Content className="content">
              <StockForm />
            </Content>
          </Layout>
        </Layout>
      </Layout>
    </BrowserRouter>
  );
};

export default DashBoard;
